package com.group4.project.cryptome;

import android.location.Location;
import java.io.Serializable;

public class EncryptedFile implements Serializable {
    private static final long serialVersionUID = 1L;
    private byte[] checkSum = null;
    private byte[] encryptedData = null;
    private Double longitude, latitude;

    // Set file unlock location.
    public void setUnlockLocation(Location unlockLocation){
        this.longitude = unlockLocation.getLongitude();
        this.latitude = unlockLocation.getLatitude();
    }

    // Get file unlock location.
    public Location getUnlockLocation(){
        Location l = new Location("");
        l.setLatitude(this.latitude);
        l.setLongitude(this.longitude);
        return l;
    }

    // Set file checksum.
    public void setCheckSum(byte[] checksum){
        this.checkSum = checksum;
    }

    // Get file checksum.
    public byte[] getCheckSum(){
        return this.checkSum;
    }

    // Set file data.
    public void setEncryptedData(byte[] encryptedData){
        this.encryptedData = encryptedData;
    }

    // Get file data.
    public byte[] getEncryptedData(){
        return this.encryptedData;
    }
}
