package com.group4.project.cryptome;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class EncryptActivity extends AppCompatActivity {
    // Request codes.
    private static final int REQUEST_ACCESS_FINE_LOCATION = 1;
    private static final int REQUEST_ACCESS_FILESYSTEM = 2;
    private static final int REQUEST_GET_FILE = 3;

    // Activity state.
    private EditText edtFilename = null;
    private EditText edtPassword = null;
    private EditText edtLongitude = null;
    private EditText edtLatitude = null;
    private Uri encryptFile = null;

    // Setup encryption activity.
    private void setupEncrypt() {
        Button btnEncrypt = (Button) findViewById(R.id.btnEncrypt);
        Button btnOpenFile = (Button) findViewById(R.id.btnOpenFile);
        Button btnGetLocation = (Button) findViewById(R.id.btnGetLocation);

        btnGetLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLocation();
            }
        });

        btnOpenFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileManager();
            }
        });

        btnEncrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startWorkingActivity();
            }
        });

        edtFilename = (EditText) findViewById(R.id.edtFilename);
        edtPassword = (EditText) findViewById(R.id.edtPassword);

        edtLongitude = (EditText) findViewById(R.id.edtLongitude);
        edtLatitude = (EditText) findViewById(R.id.edtLatitude);
    }

    // Open file manager.
    private void openFileManager() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_ACCESS_FILESYSTEM);
            return;
        }

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        startActivityForResult(Intent.createChooser(intent, "Open folder"), REQUEST_GET_FILE);
    }

    // Set user location.
    public void setLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PermissionChecker.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_ACCESS_FINE_LOCATION);
        } else {
            Locator locator = new Locator(this);
            try {
                this.edtLongitude.setText(Double.toString(locator.getLongitude()));
                this.edtLatitude.setText(Double.toString(locator.getLatitude()));
            } catch (NullPointerException e) {
                this.edtLongitude.setText(Double.toString(0.0));
                this.edtLatitude.setText(Double.toString(0.0));
            }
        }
    }

    // Initiates decryption working activity.
    private void startWorkingActivity() {
        Location encryptLocation;
        try {
            // Get location coordinates.

            CharSequence longitudeText  = edtLongitude.getText();
            String longitude = longitudeText.toString();
            double encryptLongitude = Double.valueOf(longitude);
            CharSequence latitudeText = edtLatitude.getText();
            String latitude = latitudeText.toString();
            double encryptLatitude = Double.valueOf(latitude);

            // Instantiate wrapper Location.
            encryptLocation = new Location("");
            encryptLocation.setLongitude(encryptLongitude);
            encryptLocation.setLatitude(encryptLatitude);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        Intent i = new Intent(getApplicationContext(), WorkingActivity.class);
        i.putExtra("action", Worker.ACTION.ENCRYPT);
        i.putExtra("encryptFile", this.encryptFile);
        CharSequence passwordText = edtPassword.getText();
        String password = passwordText.toString();
        i.putExtra("encryptPassword", password);
        i.putExtra("encryptLocation", encryptLocation);

        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encrypt);
        setupEncrypt();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ACCESS_FINE_LOCATION: {
                if (grantResults.length > ResourceProvider.getInt("MA==") && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    setLocation();
                    break;
            }
            case REQUEST_ACCESS_FILESYSTEM: {
                if (grantResults.length > ResourceProvider.getInt("MA==") && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    openFileManager();
                    break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_GET_FILE && resultCode == RESULT_OK && null != data) {
            encryptFile = data.getData();

            String fileName = FileHelper.getFileName(encryptFile, getApplicationContext());
            this.edtFilename.setText(fileName);
        }
    }


}

