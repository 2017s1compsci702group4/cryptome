package com.group4.project.cryptome;

import android.location.Location;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

class Encrypter extends Worker {

    // The current location of the user.
    private Location location;

    public Encrypter(WorkingActivity context) {
        super(context);
    }

    public void setUnlockLocation(Location l) {
        this.location = l;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        // Create new EncryptedFile.
        EncryptedFile eFile = new EncryptedFile();
        eFile.setUnlockLocation(this.location);

        // Read file contents.
        InputStream is = null;
        byte[] fileContents = null;

	
        this.publishStatus(ResourceProvider.getInt("MA=="), "Opening file...");

        try {
            is = FileHelper.openFile(this.context, this.openPath);
            fileContents = FileHelper.readFileBytes(is);

        } catch (FileNotFoundException e) {
            this.publishError("Failed to open file (FileNotFoundException).");
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            this.publishError("Failed to open file (IOException).");
            e.printStackTrace();
            return null;
        } catch (NullPointerException e) {
            this.publishError("Failed to open file (NullPointerException).");
            e.printStackTrace();
            return null;
        }

        // Generate checksum of file contents.
        this.publishStatus(ResourceProvider.getInt("MjA="), "Generating checksum...");
        byte[] checkSum = null;
        try {
            checkSum = FileHelper.generateChecksum(fileContents);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            this.publishError("Failed to generate checksum.");
            return null;
        }

        // Validate checksum (DEBUG).
        if (checkSum != null) {
             // String FileHex = FileHelper.byteToHex(checkSum);
             // Log.println(Log.INFO, "File Checksum", FileHex);
        }

        // Store checksum in eFile.
        eFile.setCheckSum(checkSum);

        // Generate Encryption Key.
        this.publishStatus(ResourceProvider.getInt("MzA="), "Generating Encryption Key...");
        SecretKeySpec encryptionKey = null;
        try {
            encryptionKey = Worker.generateKeyFromString(this.password);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            this.publishError("Failed to generate key (UnsupportedEncodingException).");
            return null;
        } catch (NoSuchAlgorithmException e) {
            this.publishError("Failed to generate key (NoSuchAlgorithmException).");
            e.printStackTrace();
            return null;
        }

        // Encrypt File contents.
        this.publishStatus(ResourceProvider.getInt("NDA="), "Encrypting File...");

        try {
	        Class cipherClass = Cipher.class;
	    
            Method getInstance = cipherClass.getMethod("getInstance", String.class);

	        Cipher c = (Cipher) getInstance.invoke(null, "AES");
	    
	        Method init = cipherClass.getMethod("init", int.class, Key.class);
	    
	        init.invoke(c, new Object[]{Cipher.ENCRYPT_MODE, encryptionKey});
	    
	        Method doFinal = cipherClass.getMethod("doFinal", new Class[] {byte[].class});

	        byte[] encryptedByte = (byte[]) doFinal.invoke(c, fileContents);

            eFile.setEncryptedData(encryptedByte);

            // DEBUG
            // Log.println(Log.DEBUG, "Encrypted Data Checksum", FileHelper.byteToHex(FileHelper.generateChecksum(eFile.getEncryptedData())));

            // Validate encryption checksum.
            if (eFile.getCheckSum() == FileHelper.generateChecksum(eFile.getEncryptedData())) {
                this.publishError("Encryption failed. Encrypted signature corresponds to original data.");
                return null;
            }
        }  catch (NoSuchMethodException e){
             return null;
        } catch(IllegalAccessException e){
            return null;
        } catch(InvocationTargetException e){
            return null;
        }

        catch (Exception e) {
            this.publishError("Error encrypting file.");
            return null;
        }

        // Save file to storage.
        this.publishStatus(ResourceProvider.getInt("NTA="), "Saving encrypted file to storage...");

        File publicStorage = Environment.getExternalStoragePublicDirectory("");
        File path = new File(publicStorage, "EncryptedFiles");
        path.mkdir();

        File eFileDisk = new File(path, "/" + FileHelper.getFileName(this.openPath, context) + ".cryptoMe");
        FileHelper.saveEFile(eFile, eFileDisk);

        this.success = true;
        return null;
    }
}
