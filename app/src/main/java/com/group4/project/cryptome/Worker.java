package com.group4.project.cryptome;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.spec.SecretKeySpec;

abstract class Worker extends AsyncTask<Void, Void, Void> {
    enum ACTION {ENCRYPT, DECRYPT}

    // Maximum distance from encrypt location for successful decryption.
    static int decryptRadius;

    protected WorkingActivity context;
    protected String currentActivity;

    protected Uri openPath;
    protected String password;

    protected int progress = ResourceProvider.getInt("MA==");
    protected boolean success = false;

    // Set worker file path.
    public void setOpenPath(Uri path) {
        this.openPath = path;
    }

    // Set worker password.
    public void setPassword(String password) {
        this.password = password;
    }

    // Set worker context.
    public void setContext(WorkingActivity context) {
        this.context = context;
    }

    // Instantiate a new Worker.
    public Worker(WorkingActivity context) {
        this.setContext(context);
        this.decryptRadius = ResourceProvider.getInt("NTAw");
    }

    // Returns a hardcoded salt for password generation.
    static String getSalt() { return "saltyRizwan"; }

    // Returns an AES key that can be used for encrypting and decrypting files.
    static SecretKeySpec generateKeyFromString(String password) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        // DEBUG.
        // Log.println(Log.DEBUG, "Generate Key", "Password: " + password);

        // Initialise key:
        // 1. Concatenate password with salt.
        // 2. Get SHA-1 digest.
        // 3. Truncate to 128-bits.
        MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
        String salt = getSalt();
        byte[] key = (password + salt).getBytes("UTF-8");
        key = Arrays.copyOf(sha1.digest(key), ResourceProvider.getInt("MTY="));

        return new SecretKeySpec(key, "AES");
    }

    // Helper wrappers for publishStatus.
    protected void publishError(String errorMessage, boolean success) {
        this.success = success;
        this.publishStatus(ResourceProvider.getInt("MA=="), errorMessage);
    }
    protected void publishError(String errorMessage) {
        this.publishError(errorMessage, false);
    }

    // A helper wrapper for publishProgress.
    protected void publishStatus(int progress, String message) {
        // DEBUG.
        // Log.println(Log.INFO, "Working Status", message + " | " + progress + "%");

        this.currentActivity = message;
        this.progress = progress;

        publishProgress();
    }

    @Override
    protected void onPreExecute() {
        this.context.updateStatus(ResourceProvider.getInt("MA=="), "Getting Ready");
    }

    @Override
    protected void onProgressUpdate(Void... progress) {
        this.context.updateStatus(this.progress, this.currentActivity);
    }

    @Override
    protected void onPostExecute(Void result) {
        if (this.success){
            this.context.taskCompletedCallback();
        }
        else{
            this.context.taskErrorCallback();
        }
    }
}
