/*
 * Authors:
 Will Thomson,
 Ryan Tiedemann,
 Albert Mitton,
 Song Ji,
 Kristoffer Sloth Gade
 */

package com.group4.project.cryptome;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private static Context mContext;
    public static Context getContext(){
        return MainActivity.mContext;
    }
    // Setup main activity.
    private void setupMain() {
        Button menuEncryptButton = (Button) findViewById(R.id.btnEncrypt);
        Button menuDecryptButton = (Button) findViewById(R.id.btnDecrypt);

        menuEncryptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), EncryptActivity.class);
                startActivity(i);
            }
        });

        menuDecryptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), DecryptActivity.class);
                startActivity(i);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this.getApplicationContext();
        setupMain();
    }
}

