package com.group4.project.cryptome;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class FileHelper {

    public static String getFileName(Uri uri, Context mContext) {
        String result = null;
        String Scheme = uri.getScheme();
        Boolean methodResult = Scheme.equals("content");
        if (methodResult)
        {
            Cursor cursor = mContext.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != ResourceProvider.getInt("LTE=")) {
                result = result.substring(cut + ResourceProvider.getInt("MQ=="));
            }
        }
        return result;
    }

    public static void saveFile(File outFile, byte[] decryptedData) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(outFile);
        fileOutputStream.write(decryptedData);
        fileOutputStream.close();
    }

    public static InputStream openFile(WorkingActivity context, Uri filepath) throws FileNotFoundException {
        return context.getContentResolver().openInputStream(filepath);
    }

    public static byte[] readFileBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = ResourceProvider.getInt("MTAyNA==");
        byte[] buffer = new byte[bufferSize];
        int len = inputStream.read(buffer);
        while (len != ResourceProvider.getInt("LTE=")) {
            byteBuffer.write(buffer, ResourceProvider.getInt("MA=="), len);
            len = inputStream.read(buffer);
        }
        return byteBuffer.toByteArray();
    }

    public static void saveEFile(EncryptedFile eFile, File oFile) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(oFile);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(eFile);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static EncryptedFile readEFile(WorkingActivity context, Uri filepath) throws IOException, ClassNotFoundException {
        InputStream is;
        ObjectInputStream os;
        EncryptedFile eFile;

        is = FileHelper.openFile(context, filepath);
        os = new ObjectInputStream(is);
        eFile = (EncryptedFile) os.readObject();
        is.close();

        return eFile;
    }

    public static String byteToHex(byte[] bytes) {
        String result = "";
        for (int i = ResourceProvider.getInt("MA=="); i < bytes.length; i++) {
            result += Integer.toString((bytes[i] & 0xff) + 0x100, ResourceProvider.getInt("MTY=")).substring(ResourceProvider.getInt("MQ=="));
        }
        return result;
    }

    public static byte[] generateChecksum(byte[] data) throws NoSuchAlgorithmException {
        MessageDigest md = null;
        md = MessageDigest.getInstance("MD5");
        return md.digest(data);
    }
}
