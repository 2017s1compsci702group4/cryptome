package com.group4.project.cryptome;

import android.Manifest;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;

public class Locator {
    private int minTime;
    private int minDistance;
    private int maxLongDP;
    private int maxLatDP;

    private final Context mContext;

    public Locator(Context mContext) {
        this.mContext = mContext;
        this.minTime = ResourceProvider.getInt("NTAw");
        this.minDistance = ResourceProvider.getInt("MTA=");
        this.maxLongDP = ResourceProvider.getInt("Mg==");
        this.maxLatDP = ResourceProvider.getInt("Mg==");
    }

    public Location getLocation() {
        LocationManager locationManager = (LocationManager)mContext.getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PermissionChecker.PERMISSION_GRANTED)
            return new Location("");

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime, minDistance, new LocationListener() {
            @Override public void onLocationChanged(Location location) {}
            @Override public void onStatusChanged(String provider, int status, Bundle extras) {}
            @Override public void onProviderEnabled(String provider) {}
            @Override public void onProviderDisabled(String provider) {}
        });

        return locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
    }

    public double getLongitude() {
        Location location = this.getLocation();
        double longitude = location.getLongitude();
        return (double) (int) (longitude * Math.pow(ResourceProvider.getInt("MTA="), maxLongDP)) / Math.pow(ResourceProvider.getInt("MTA="), maxLongDP);
    }

    public double getLatitude() {
        Location location = this.getLocation();
        double latitude = location.getLatitude();
        return (double)(int)(latitude*Math.pow(ResourceProvider.getInt("MTA="), maxLatDP))/Math.pow(ResourceProvider.getInt("MTA="), maxLatDP);
    }

    public Location getLocationObject(){
        Location location = new Location("");
        double latitude = this.getLatitude();
        double longitude = this.getLongitude();
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        return location;
    }
}
