package com.group4.project.cryptome;

import android.location.Location;
import android.os.Environment;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

class Decrypter extends Worker {

    // The current location of the user.
    private Location currentLocation;

    public Decrypter(WorkingActivity context) {
        super(context);
    }

    public void setCurrentLocation(Location location) {
        this.currentLocation = location;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        EncryptedFile eFile = null;

        this.publishStatus(ResourceProvider.getInt("MTA="), "Loading encrypted file...");
        try {
            eFile = FileHelper.readEFile(this.context, this.openPath);
        } catch (IOException e) {
            e.printStackTrace();
            this.publishError("Failed to load encrypted file IOException.");
            return null;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            this.publishError("Failed to load encrypted file ClassNotFoundException.");
            return null;
        }

        // Verify user is in valid location to decrypt file.
        Location file  = eFile.getUnlockLocation();
        double currentDistance = currentLocation.distanceTo(file);
        if (currentDistance > Worker.decryptRadius) {
            this.publishError("You are not in a valid location to decrypt this file.");
            return null;
        }

        // Generate Decryption key.
        SecretKeySpec decryptionKey = null;

        this.publishStatus(ResourceProvider.getInt("MjA="), "Generating Decryption Key...");
        try {
            decryptionKey = Worker.generateKeyFromString(this.password);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            this.publishError("Failed to generate key UnsupportedEncodingException.");
            return null;
        } catch (NoSuchAlgorithmException e) {
            this.publishError("Failed to generate key NoSuchAlgorithmException.");
            e.printStackTrace();
            return null;
        }

        // Decrypt Data.
        Cipher c = null;
        byte[] decryptedData = null;

        this.publishStatus(ResourceProvider.getInt("MzA="), "Decrypting Data");
        try {
            c = Cipher.getInstance("AES");
            c.init(Cipher.DECRYPT_MODE, decryptionKey);

            byte[] fileData = eFile.getEncryptedData();
            decryptedData = c.doFinal(fileData);
        } catch (Exception e) {
            e.printStackTrace();
            this.publishError("Failed to decrypt data. Please try again.");
            return null;
        }

        // Generate and verify checksums.
        byte[] decryptedDataChecksum = null;

        this.publishStatus(ResourceProvider.getInt("NzA="), "Generating checksums...");
        try {
            decryptedDataChecksum = FileHelper.generateChecksum(decryptedData);
        } catch (Exception e) {
            e.printStackTrace();
            this.publishError("Failed to Generate Checksum.");
            return null;
        }

        this.publishStatus(ResourceProvider.getInt("ODA="), "Verifying Checksums...");
        byte[] efile = eFile.getCheckSum();
        String fileHex = FileHelper.byteToHex(decryptedDataChecksum);
        String fileSumHex = FileHelper.byteToHex(efile);
        Boolean isEqual = fileHex.equals(fileSumHex);
        if (!isEqual) {
            this.publishError("Corrupted data detected!");
            return null;
        }

        // Generate storage folder.
        this.publishStatus(ResourceProvider.getInt("OTA="), "Generating Storage folder...");
        File publicStorage = Environment.getExternalStoragePublicDirectory("");
        File path = new File(publicStorage, "DecryptedFiles");
        path.mkdir();

        // Write data to storage.
        String filename = FileHelper.getFileName(this.openPath, context);

        this.publishStatus(ResourceProvider.getInt("OTU="), "Writing data to storage...");
        int filelength = filename.length();
        String fileString = filename.substring(ResourceProvider.getInt("MA=="),filelength - ResourceProvider.getInt("OQ=="));
        File decryptedFile = new File(path, "/" + fileString);
        try {
            FileHelper.saveFile(decryptedFile, decryptedData);
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.success = true;
        return null;
    }
}
