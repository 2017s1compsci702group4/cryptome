package com.group4.project.cryptome;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.File;

public class DecryptActivity extends AppCompatActivity {
    // Request codes.
    private static final int REQUEST_ACCESS_FINE_LOCATION = 1;
    private static final int REQUEST_ACCESS_FILESYSTEM = 2;
    private static final int REQUEST_GET_FILE = 3;

    // Activity state.
    private EditText edtFilename = null;
    private EditText edtPassword = null;
    private Uri decryptFile = null;

    // Setup decrypt activity.
    private void setupDecrypt() {
        Button decryptButton = (Button) findViewById(R.id.btnDecrypt);
        Button openFileButton = (Button) findViewById(R.id.btnOpenFile);

        decryptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startWorkingActivity();
            }
        });

        openFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFileManager();
            }
        });

        edtFilename = (EditText) findViewById(R.id.edtFilename);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
    }

    // Open file manager.
    private void openFileManager() {
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PermissionChecker.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_ACCESS_FILESYSTEM);
            return;
        }

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        Intent Chooser  = Intent.createChooser(intent, "Open folder");
        startActivityForResult(Chooser, REQUEST_GET_FILE);
    }

    // Wrapper for checking location permissions.
    private boolean checkLocationPermissions(){
        boolean permissionCheckResult = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PermissionChecker.PERMISSION_GRANTED;
        return permissionCheckResult;
    }

    // Wrapper for requesting location permissions.
    private void requestLocationPermission(){
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},REQUEST_ACCESS_FINE_LOCATION);
    }

    // Initiates decryption working activity.
    private void startWorkingActivity(){
        if(!checkLocationPermissions()){
            requestLocationPermission();
            return;
        }

        Intent i = new Intent(getApplicationContext(), WorkingActivity.class);
        i.putExtra("action", Worker.ACTION.DECRYPT);
        i.putExtra("decryptFile", this.decryptFile);
        CharSequence passwordText = edtPassword.getText();
        String password = passwordText.toString();
        i.putExtra("decryptPassword", password);
        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decrypt);
        setupDecrypt();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ACCESS_FINE_LOCATION: {
                if (grantResults.length > ResourceProvider.getInt("MA==") && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    startWorkingActivity();
            }

            case REQUEST_ACCESS_FILESYSTEM: {
                if (grantResults.length > ResourceProvider.getInt("MA==") && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    openFileManager();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_GET_FILE && resultCode == RESULT_OK && data != null) {
            decryptFile = data.getData();
            Context ctx = getApplicationContext();
            String fileName = FileHelper.getFileName(decryptFile, ctx);
            this.edtFilename.setText(fileName);
        }
    }
}
