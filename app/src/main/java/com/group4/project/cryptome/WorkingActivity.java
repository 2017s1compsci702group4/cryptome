package com.group4.project.cryptome;

import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class WorkingActivity extends AppCompatActivity {
    private Button doneButton;
    private ProgressBar progressBar;
    private TextView percentageText;
    private TextView statusText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        Intent i = getIntent();

        Worker.ACTION action = (Worker.ACTION) i.getSerializableExtra("action");

        Uri fileUri = null;
        String encryptPassword = null;
        String decryptPassword = null;
        Location encryptLocation = null;

        // Extract parcelled data.
        if(action == Worker.ACTION.ENCRYPT) {
            fileUri = i.getParcelableExtra("encryptFile");
            encryptPassword = i.getStringExtra("encryptPassword");
            encryptLocation = i.getParcelableExtra("encryptLocation");
        }
        else if(action == Worker.ACTION.DECRYPT){
            fileUri = i.getParcelableExtra("decryptFile");
            decryptPassword = i.getStringExtra("decryptPassword");
        }

        // Setup the done button.
        this.doneButton = (Button) findViewById(R.id.btnDone);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
            }
        });
        disableButton(doneButton);

        // Setup progress bar.
        this.progressBar = (ProgressBar) findViewById(R.id.barProgress);
        this.progressBar.setMax(ResourceProvider.getInt("MTAw"));

        // Setup percentage view.
        this.percentageText = (TextView) findViewById(R.id.txtProgress);

        // Setup current activity text view.
        this.statusText = (TextView) findViewById(R.id.txtStatus);

        // Update the starting status.
        this.updateStatus(ResourceProvider.getInt("MA=="), "Preparing to " + (action == Worker.ACTION.ENCRYPT ? "Encrypt" : "Decrypt"));

        // Start the correct job.
        if(action == Worker.ACTION.ENCRYPT){
            this.startEncrypt(fileUri, encryptLocation, encryptPassword);
        } else if(action == Worker.ACTION.DECRYPT){
            this.startDecrypt(fileUri, decryptPassword);
        }
    }

    // Enable UI button.
    private void enableButton(Button btn){
        btn.setAlpha(1f);
        btn.setClickable(true);
    }

    // Disable UI button.
    private void disableButton(Button btn){
        btn.setAlpha(.5f);
        btn.setClickable(false);
    }

    // Execute encryption process.
    private void startEncrypt(Uri fileUri, Location encryptLocation, String encryptPassword){
        Encrypter encrypter = new Encrypter(this);
        encrypter.setOpenPath(fileUri);
        encrypter.setUnlockLocation(encryptLocation);
        encrypter.setPassword(encryptPassword);
        encrypter.execute();
        return;
    }

    // Execute decryption process.
    private void startDecrypt(Uri fileUri, String decryptPassword){
        Locator locator = new Locator(this);
        Location currentLocation = locator.getLocationObject();

        Decrypter decrypter = new Decrypter(this);
        decrypter.setCurrentLocation(currentLocation);
        decrypter.setOpenPath(fileUri);
        decrypter.setPassword(decryptPassword);
        decrypter.execute();
        return;
    }

    // Update process status.
    protected void updateStatus(int progress, String activity){
        this.progressBar.setProgress(progress);
        this.percentageText.setText(progress + "%");
        this.statusText.setText(activity);
    }

    // Call in case of task completion.
    protected void taskCompletedCallback(){
        this.updateStatus(ResourceProvider.getInt("MTAw"), "Done!");
        this.enableButton(this.doneButton);
    }

    // Call in case of task error.
    protected void taskErrorCallback(){
        this.doneButton.setText("Return to menu");
        this.enableButton(this.doneButton);
    }
}
