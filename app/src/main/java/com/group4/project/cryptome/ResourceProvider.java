package com.group4.project.cryptome;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class ResourceProvider {

    private final static String base64chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    static String getString(String str){
        String data = null;
        try {
            Signature[] signatures = getSignatures(MainActivity.getContext());
            String key = ResourceProvider.shiftAsciiCharacters(signatures[0].toCharsString(), 30);
	        //String key = ResourceProvider.shiftAsciiCharacters(sig, 30);
            data = getIntString(key, str);

	    } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
	    }
        return data;
    }

    static String getIntString(String key, String str){
        String shifted = ResourceProvider.shiftAsciiCharacters(str, 30, false);
        byte[] data2 = Base64.decode(shifted, Base64.DEFAULT);
        byte[] byteData2 = ResourceProvider.xorWithKey(data2, key.getBytes());
        return new String(byteData2);
    }

    static byte[] xorWithKey(byte[] a, byte[] key) {
        byte[] out = new byte[a.length];
        for (int i = 0; i < a.length; i++) {
            out[i] = (byte) (a[i] ^ key[i%key.length]);
        }
        return out;
    }
    static String shiftAsciiCharacters(String str, int shift){
        return ResourceProvider.shiftAsciiCharacters(str, shift, true);
    }
    static String shiftAsciiCharacters(String str, int shift, boolean plus){
        char[] chars = str.toCharArray();
        for(int i=0; i<chars.length; i++){
            int ascii = chars[i];
            int newAscii;
            if(plus){
                newAscii = ascii + (shift + i);
                plus = false;
            }else{
                newAscii = ascii - (shift + i);
                plus = true;
            }
            while(newAscii < 32){
                newAscii = 127 - (32 - newAscii);
            }
            while(newAscii > 126){
                newAscii = 31 + (newAscii - 126);
            }
            chars[i] = (char) newAscii;

        }
        return String.valueOf(chars);
    }

    static private Signature[] getSignatures(Context context) throws PackageManager.NameNotFoundException {
        return context.getPackageManager().getPackageInfo(PackageName.getPackageName(), PackageManager.GET_SIGNATURES).signatures;
    }

    public static int getInt(String s) {
        // Only characters in Base64 are used, ignore others.
        s = s.replaceAll("[^" + base64chars + "=]", "");

        // Zero padding, A will also be a zero.
        String p = (s.charAt(s.length() - 1) == '=' ?
                (s.charAt(s.length() - 2) == '=' ? "AA" : "A") : "");
        String r = "";
        s = s.substring(0, s.length() - p.length()) + p;

        // Incrementation of this encoded string over the length, steps of 4 characters at a time.
        for (int c = 0; c < s.length(); c += 4) {

            int n = (base64chars.indexOf(s.charAt(c)) << 18)
                    + (base64chars.indexOf(s.charAt(c + 1)) << 12)
                    + (base64chars.indexOf(s.charAt(c + 2)) << 6)
                    + base64chars.indexOf(s.charAt(c + 3));

            // Split into ASCII.
            r += "" + (char) ((n >>> 16) & 0xFF) + (char) ((n >>> 8) & 0xFF)
                    + (char) (n & 0xFF);
        }

        // Zero padding is removed.
	    s = r.substring(0, r.length() - p.length());
        try {
            Method parseInt = Integer.class.getMethod("parseInt", String.class);
            return (int) parseInt.invoke(null, s);
        } catch (NoSuchMethodException e){
            return 0;
        } catch(IllegalAccessException e){
            return 0;
        } catch(InvocationTargetException e){
            return 0;
        }
    }


}
