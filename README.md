# CryptoMe

CryptoMe offers easy and safe encryption of your file on your smart-device.

As well as password encryption, CryptoMe secures your files using a novel location-based security concept which
prevents your files being accessed anywhere except for the location you choose.

Note: CryptoMe is compatible with API 21 onwards.

##Encryption:

To encrypt a file, click the "Encrypt" button.

At the encryption screen, you can browse your device for files you wish to encrypt, and enter your desired password and decryption location.
Your encrypted file will be saved in your device storage, under the "EncryptedFiles" directory.

##Decryption:

To decrypt a file, click the "Decrypt" button.

At the decryption screen, browse the EncryptedFiles directory for the file you wish to decrypt, and enter your password.
Your decrypted file will be saved in your device storage, under the "DecryptedFiles" directory.

##Files included:

CryptoMe.apk - the CryptoMe application.

